#import "MNAppDelegate.h"

#import <CocoaLumberjack/DDLog.h>
#import <CocoaLumberjack/DDASLLogger.h>
#import <CocoaLumberjack/DDTTYLogger.h>

#import "MNViewController.h"

#import "MNIMAPClient.h"
#import "MNNetworkConnection.h"
#import "MNTCPConnection.h"
#import "MNTCPHost.h"

static const int ddLogLevel = LOG_LEVEL_VERBOSE;

@interface MNAppDelegate ()
@property(nonatomic, strong) MNIMAPClient *imap;
@end

@implementation MNAppDelegate

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  
  [DDLog addLogger:[DDASLLogger sharedInstance]];
  [DDLog addLogger:[DDTTYLogger sharedInstance]];
  
  self.imap = [MNIMAPClient clientWithNetworkConnectionFactory:^{
    MNTCPHost *gmailIMAPHost = [MNTCPHost hostWithName:@"imap.gmail.com"];
    NSUInteger TLSIMAPPortNumber = 993;
    return [MNTCPConnection connectionWithHost:gmailIMAPHost onPort:TLSIMAPPortNumber];
  }];
  
  [self.imap capability:^(NSArray *capabilities, NSError *error){
    for (NSString *capability in capabilities) {
      DDLogInfo(@"Capability: %@", capability);
    }
  }];

  MNAppDelegate __weak * weakSelf = self;
  MNIMAPLoginResponse loginResponse = ^{
    DDLogInfo(@"Logged In!");
    [weakSelf.imap select:^{
      DDLogInfo(@"Selected Inbox!");
      [weakSelf.imap search:^(NSArray *messageSequenceNumbers){
        DDLogVerbose(@"Today's Messages: %@", messageSequenceNumbers);
        
        for (NSString *messageSequenceNumber in messageSequenceNumbers) {
          [weakSelf.imap fetch:^(NSString *subject){
            DDLogVerbose(@"Subject:\n%@", subject);
          }
                   sequenceSet:messageSequenceNumber
   messageDataItemNamesOrMacro:@"BODY.PEEK[HEADER.FIELDS (SUBJECT)]"];
        }
      } searchCriteria:@"SENTON 13-Apr-2013"];
    
    } mailbox:@"INBOX"];
  };
  
  [self.imap login:loginResponse
      withUsername:@"rene.cacheaux@gmail.com"
       andPassword:@"Rc#8288756"];

  self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
  // Override point for customization after application launch.
  self.viewController = [[MNViewController alloc] initWithNibName:@"MNViewController"
                                                           bundle:nil];
  self.window.rootViewController = self.viewController;
  [self.window makeKeyAndVisible];
  return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
  // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
  // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
  // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
  // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
  // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
  // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
  // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
