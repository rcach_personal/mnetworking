//
//  main.m
//  MNetwork
//
//  Created by Rene Cacheaux on 2/24/13.
//  Copyright (c) 2013 RCach Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MNAppDelegate.h"

int main(int argc, char *argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([MNAppDelegate class]));
  }
}
