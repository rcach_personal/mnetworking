// MNFail causes assertion failure in DEBUG configurations.
#if DEBUG
  #define MNFail(format, ...) \
    do { \
      NSString *func = [NSString stringWithUTF8String:__PRETTY_FUNCTION__]; \
      NSString *file = [NSString stringWithUTF8String:__FILE__]; \
      [[NSAssertionHandler currentHandler] \
          handleFailureInFunction:func \
                             file:file \
                       lineNumber:__LINE__ \
                      description:format, ##__VA_ARGS__];\
    } while (0)
#else
  #define MNFail(format, ...) do {} while (0)
#endif


// MNCheck asserts that a condition is true.
#define MNCheck(condition, format, ...) \
  do { \
    if (!(condition)) { \
      MNFail(format, ##__VA_ARGS__); \
    } \
  } while (0)


// TODO: Docs.
#if DEBUG
  #define MNAssert(condition, format, ...) \
    MNCheck(condition, format, ##__VA_ARGS__)
#else
  #define MNAssert(condition, format, ...) do {} while (0)
#endif


// Invalidates initializeer from which it is called.
#ifndef MNInvalidateInitializer
  #if __has_feature(objc_arc)
    #define MNInvalidateInitializer() \
        do { \
          [self class]; /* Avoid dead store to self warning. */ \
          MNAssert(NO, @"Invalid initializer."); \
          return nil; \
        } while (0)
  #else
    #define MNInvalidateInitializer() \
        do { \
          [self release]; \
          MNAssert(NO, @"Invalid initializer."); \
          return nil; \
        } while (0)
  #endif
#endif

// For abstract classes.
// Use for methods that must be overriden.
#define MNAbstractMethod \
  MNFail(@"%@ must implement %@", \
         NSStringFromClass([self class]), \
         NSStringFromSelector(_cmd))

// Place after calling |super| in the initializer . Asserts in debug.
#define MNAbstractClass(clazz) \
  do { \
    if ([self isMemberOfClass:[clazz class]]) { \
      MNFail(@"Cannot instantiate abstract class " #clazz); \
      return nil; \
    } \
  } while (0)


