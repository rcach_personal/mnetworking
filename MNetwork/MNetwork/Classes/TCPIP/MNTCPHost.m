#import "MNTCPHost.h"

#import <CocoaLumberjack/DDLog.h>

#import "MNTCPConnection.h"

static const int ddLogLevel = LOG_LEVEL_ERROR;

@interface MNTCPHost ()
@property(nonatomic, copy, readwrite) NSString *hostname;
@end

@implementation MNTCPHost

+ (id)hostWithName:(NSString *)hostname {
  return [[self alloc] initWithName:hostname];
}

- (id)init {
  self = [super init];
  if (self) {
    MNInvalidateInitializer();
  }
  return self;
}

- (id)initWithName:(NSString *)hostname {
  self = [super init];
  if (self) {
    self.hostname = hostname;
  }
  return self;
}

- (MNTCPConnection *)connectToPort:(NSUInteger)portNumber
                  withEventHandler:(id<MNNetworkConnectionEventHandler>)eventHandler {
  MNTCPConnection *connection = [MNTCPConnection connectionWithHost:self
                                                             onPort:portNumber];
  connection.eventHandler = eventHandler;
  [connection connect];
  return connection;
}

- (id)copyWithZone:(NSZone *)zone {
  DDLogVerbose(@"Copying Host: %@", self);
  return [[[self class] allocWithZone:zone] initWithName:[self.hostname copy]];
}

- (NSString *)description {
  return [NSString stringWithFormat:@"TCP Host with name: %@", self.hostname];
}

@end
