@class MNTCPConnection;
@protocol MNNetworkConnectionEventHandler;

@interface MNTCPHost : NSObject<NSCopying>

@property(nonatomic, copy, readonly) NSString *hostname;

+ (id)hostWithName:(NSString *)hostname;
- (id)initWithName:(NSString *)hostname;
- (MNTCPConnection *)connectToPort:(NSUInteger)portNumber
                  withEventHandler:(id<MNNetworkConnectionEventHandler>)eventHandler;

@end
