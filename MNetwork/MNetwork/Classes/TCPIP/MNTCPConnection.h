#import "MNNetworkConnection.h"

@class MNTCPHost;

@interface MNTCPConnection : NSObject<MNNetworkConnection>

+ (id)connectionWithHost:(MNTCPHost *)host onPort:(NSUInteger)portNumber;
- (id)initWithHost:(MNTCPHost *)host onPort:(NSUInteger)portNumber;

@end