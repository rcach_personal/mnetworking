#import <SenTestingKit/SenTestingKit.h>

#import <OCMock/OCMock.h>

#import "MNMockingTestCase.h"
#import "MNTCPHost.h"
#import "MNTCPConnection.h"

@interface MNTCPHostTests : MNMockingTestCase
@property(nonatomic, copy) NSString *gmailIMAPHostname;
@property(nonatomic, strong) MNTCPHost *gmailIMAPHost;
@property(nonatomic, assign) NSUInteger gmailIMAPPortNumber;
DECLARE_MOCK(connectionEventHandler)
@end

@implementation MNTCPHostTests

NICE_MOCK_PROTOCOL(connectionEventHandler, MNTCPConnectionEventHandler);

- (void)setUp {
  [super setUp];
  self.gmailIMAPHostname = @"imap.gmail.com";
  self.gmailIMAPHost = [MNTCPHost hostWithName:self.gmailIMAPHostname];
  self.gmailIMAPPortNumber = 993;
}

- (void)testHostCreation {
  MNTCPHost *host = [[MNTCPHost alloc] initWithName:self.gmailIMAPHostname];
  STAssertNotNil(host, @"");
}

- (void)testInvalidInitializer {
  MNTCPHost *host;
  STAssertThrows(host = [[MNTCPHost alloc] init], @"");
}

- (void)testFactoryMethod {
  MNTCPHost *host = [MNTCPHost hostWithName:self.gmailIMAPHostname];
  STAssertNotNil(host, @"");
}

- (void)testNewConnection {
  [[self.connectionEventHandler expect] connectionDidConnect:[OCMArg any]];
  MNTCPConnection *tcpConnection =
      [self.gmailIMAPHost connectToPort:self.gmailIMAPPortNumber
                       withEventHandler:self.connectionEventHandler];
  
  while (tcpConnection.connected == NO && tcpConnection.errorConnecting == NO) {
    [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                             beforeDate:[NSDate distantFuture]];
  }
  STAssertNotNil(tcpConnection, @"");
  tcpConnection.eventHandler = nil;
}

- (void)testRequestResponse {
  [[self.connectionEventHandler expect] connection:[OCMArg any]
                                didReceiveResponse:[OCMArg any]];
  
  MNTCPConnection *tcpConnection =
      [self.gmailIMAPHost connectToPort:self.gmailIMAPPortNumber
                       withEventHandler:self.connectionEventHandler];
  while (tcpConnection.connected == NO && tcpConnection.errorConnecting == NO) {
    [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                             beforeDate:[NSDate distantFuture]];
  }
  
  NSData *requestData;
  [tcpConnection sendRequest:requestData];
  tcpConnection.eventHandler = nil;
}

- (void)testRequestResponseData {
  MNTCPConnection *tcpConnection =
      [self.gmailIMAPHost connectToPort:self.gmailIMAPPortNumber
                       withEventHandler:self.connectionEventHandler];
  
  
  [(id<MNTCPConnectionEventHandler>)
      [self.connectionEventHandler expect] connection:tcpConnection
                                   didReceiveResponse:[OCMArg isNotNil]];
  
  while (tcpConnection.connected == NO && tcpConnection.errorConnecting == NO) {
    [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                             beforeDate:[NSDate distantFuture]];
  }
  
  [tcpConnection sendRequest:nil];
  tcpConnection.eventHandler = nil;
}
 



@end
