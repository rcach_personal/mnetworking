#import "MNTCPConnection.h"

#import <CocoaLumberjack/DDLog.h>
#import <CFNetwork/CFNetwork.h>

#import "MNTCPHost.h"
#import "MNNetworkConnection.h"

static const int ddLogLevel = LOG_LEVEL_INFO;

@interface MNTCPConnection ()<NSStreamDelegate>
@property(nonatomic, copy) MNTCPHost *host;
@property(nonatomic, assign) NSUInteger portNumber;

@property(nonatomic, assign, readwrite) BOOL connected;
@property(nonatomic, assign, readwrite) BOOL errorConnecting;
@property(nonatomic, strong, readwrite) NSError *connectionError;

@property(nonatomic, assign) BOOL readyToWrite;

@property(nonatomic, assign) BOOL responseStreamOpen;
@property(nonatomic, assign) BOOL requestStreamOpen;

@property(nonatomic, strong) NSInputStream *responseStream;
@property(nonatomic, strong) NSOutputStream *requestStream;
@end

@implementation MNTCPConnection
@synthesize eventHandler;

+ (id)connectionWithHost:(MNTCPHost *)host onPort:(NSUInteger)portNumber {
  return [[self alloc] initWithHost:host onPort:portNumber];
}

- (id)init {
  MNInvalidateInitializer();
}

- (id)initWithHost:(MNTCPHost *)host onPort:(NSUInteger)portNumber {
  self = [super init];
  if (self) {
    [self resetState];
    self.host = host;
    self.portNumber = portNumber;
  }
  return self;
}

- (void)connect {
  // TODO: Handle connection errors, tell the event handler and set the connection error
  // property (oh and update state).
  
  if (![self createRequestAndResponseStreamWithSocketToHost:self.host
                                                     onPort:self.portNumber]) {
    // TODO: Handle error.
    DDLogError(@"TCP Connection: ERROR creating socket streams for host, %@, on port, %d",
               self.host,
               self.portNumber);
  } else {
    // TODO: Only apply this if consumer asks for it.
    [self turnOnTLS];
    [self scheduleStreamsOnMainRunLoop];
    [self openStreams];
  }
}

- (void)disconnect {
  [self closeStreams];
  [self removeStreamsFromMainRunLoop];
  [self nilOutStreams];
  [self updateStateForClosedStreams];
  DDLogVerbose(@"TCP Connection: Connection, %@, Disconnected", self);
  if (self.eventHandler) {
    [self.eventHandler connectionDidDisconnect:self error:nil];
  }
}

- (BOOL)sendData:(NSData *)request {
  if (self.readyToWrite) {
    const uint8_t *buf = (const uint8_t*)[request bytes];
    NSInteger numberOfBytesInRequest = [request length];
    
    return [self writeToRequestStreamWithBytes:buf
                        numberOfBytesInRequest:numberOfBytesInRequest];
  } else {
    return NO;
  }
}

#pragma mark - NSStream Delegate

- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode {
  DDLogVerbose(@"TCP Connection: Received NSStrem event, %d.", eventCode);
  if (aStream == self.responseStream) {
    [self handleResponseStreamEvent:eventCode];
  } else if (aStream == self.requestStream) {
    [self handleRequestStreamEvent:eventCode];
  }
}

- (void)handleRequestStreamEvent:(NSStreamEvent)eventCode {
  switch (eventCode) {
    case NSStreamEventOpenCompleted:
      DDLogVerbose(@"TCP Connection: WRITE CONNECTED");
      self.requestStreamOpen = YES;
      break;
      
    case NSStreamEventHasSpaceAvailable:
      DDLogVerbose(@"TCP Connection: Ready to WRITE");
      self.readyToWrite = YES;
      [self updateConnectionStatus];
      break;
      
    case NSStreamEventErrorOccurred:
      [self handleErrorForStream:self.requestStream];
      break;
    
    default:
      break;
  }
}

- (void)handleResponseStreamEvent:(NSStreamEvent)eventCode {
  switch (eventCode) {
    case NSStreamEventOpenCompleted:
      DDLogVerbose(@"TCP Connection: READ CONNECTED");
      self.responseStreamOpen = YES;
      break;
      
    case NSStreamEventHasBytesAvailable: 
      DDLogVerbose(@"TCP Connection: Ready to READ");
      [self readResponse];
      break;
    
    case NSStreamEventErrorOccurred:
      [self handleErrorForStream:self.responseStream];
      break;
      
    default:
      break;
  }
}

#pragma mark - Read from and Write to Stream

- (BOOL)writeToRequestStreamWithBytes:(const uint8_t *)buf
               numberOfBytesInRequest:(NSInteger)numberOfBytesInRequest {
  NSInteger bytesWritten = 0;
  while (bytesWritten < numberOfBytesInRequest) {
    NSInteger result =
    [self.requestStream write:(buf + bytesWritten)
                    maxLength:(numberOfBytesInRequest - bytesWritten)];
    if (result <= 0) {
      DDLogError(@"NSOutputStream write returned %ld", (long)result);
      return NO;
    }
    bytesWritten += result;
  }
  BOOL requestSent = bytesWritten > 0;
  return requestSent;
}

- (void)readResponse {
  NSData *responseData;
  UInt8 buf[BUFSIZ];
  NSInteger numberBytesRead = [self.responseStream read:buf maxLength:BUFSIZ];
  if (numberBytesRead > 0) {
    responseData = [NSData dataWithBytes:buf length:numberBytesRead];
    if (self.eventHandler) {
      [self.eventHandler connection:self didReceiveData:responseData];
    }
  }
}

#pragma mark - Stream Utilities

- (BOOL)createRequestAndResponseStreamWithSocketToHost:(MNTCPHost *)host
                                                onPort:(NSUInteger)portNumber {
  CFWriteStreamRef requestStream = NULL;
  CFReadStreamRef responseStream = NULL;
  // Create Host.
  CFHostRef cfhost = CFHostCreateWithName(kCFAllocatorDefault,
                                          (__bridge CFStringRef) host.hostname);
  
  if (cfhost) {
    // Create Streams.
    // TODO: Map port number to transport sec. Allow clients to override.
    CFStreamCreatePairWithSocketToCFHost(kCFAllocatorDefault,
                                         cfhost,
                                         portNumber,
                                         &responseStream,
                                         &requestStream);
    CFRelease(cfhost);
  }
  
  if (responseStream && requestStream) {
    self.requestStream = CFBridgingRelease(requestStream);
    self.responseStream = CFBridgingRelease(responseStream);
    return YES;
  } else {
    return NO;
  }
}

- (void)turnOnTLS {
  [self.requestStream setProperty:(NSString *)kCFStreamSocketSecurityLevelTLSv1
                           forKey:(NSString *)kCFStreamPropertySocketSecurityLevel];
  [self.responseStream setProperty:(NSString *)kCFStreamSocketSecurityLevelTLSv1
                            forKey:(NSString *)kCFStreamPropertySocketSecurityLevel];
}

- (void)scheduleStreamsOnMainRunLoop {
  self.responseStream.delegate = self;
  [self.responseStream scheduleInRunLoop:[NSRunLoop currentRunLoop]
                                 forMode:NSRunLoopCommonModes];
  self.requestStream.delegate = self;
  [self.requestStream scheduleInRunLoop:[NSRunLoop currentRunLoop]
                                forMode:NSRunLoopCommonModes];
}

- (void)removeStreamsFromMainRunLoop {
  [self.requestStream removeFromRunLoop:[NSRunLoop currentRunLoop]
                                forMode:NSRunLoopCommonModes];
  [self.responseStream removeFromRunLoop:[NSRunLoop currentRunLoop]
                                 forMode:NSRunLoopCommonModes];
}

- (void)openStreams {
  DDLogVerbose(@"TCP Connection: Opening response stream, %@.", self.responseStream);
  [self.responseStream open];
  DDLogVerbose(@"TCP Connection: Opening request stream, %@.", self.requestStream);
  [self.requestStream open];
}

- (void)closeStreams {
  [self.requestStream close];
  [self.responseStream close];
}

- (void)nilOutStreams {
  self.requestStream = nil;
  self.responseStream = nil;
}

- (void)handleErrorForStream:(NSStream *)stream {
  // TODO: This isn't necessarily a connecting error.
  self.errorConnecting = YES;
  NSError *error = [stream streamError];
  DDLogError(@"Error %@", [error localizedDescription]);
  [stream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
  [stream close];
  stream = nil;
  
  if (stream == self.requestStream) {
    self.requestStreamOpen = NO;
    self.readyToWrite = NO;
  } else if (stream == self.responseStream) {
    self.responseStream = nil;
    self.responseStreamOpen = NO;
  }
}

#pragma mark - State

- (void)updateConnectionStatus {
  if (self.responseStreamOpen && self.requestStreamOpen) {
    if (!self.connected) {
      if (self.eventHandler) {
        [self.eventHandler connectionDidConnect:self];
      }
    }
    self.connected = YES;
  } else {
    self.connected = NO;
  }
}

- (void)resetState {
  self.connected = NO;
  self.errorConnecting = NO;
  self.readyToWrite = NO;
  self.responseStreamOpen = NO;
  self.requestStreamOpen = NO;
}

- (void)updateStateForClosedStreams {
  self.readyToWrite = NO;
  self.requestStreamOpen = NO;
  self.responseStreamOpen = NO;
  self.connected = NO;
}

@end
