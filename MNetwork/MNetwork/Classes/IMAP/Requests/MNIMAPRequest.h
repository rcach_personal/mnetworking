@class MNIMAPRequestTicket;
@class MNIMAPResponseLine;

typedef NS_ENUM(NSUInteger, MNIMAPRequestStatus) {
  MNIMAPRequestStatusUnknown,
  MNIMAPRequestStatusPending,
  MNIMAPRequestStatusInProgress,
  MNIMAPRequestStatusComplete,
};

// TODO: Consider calling this IMAPCommand instead. IMAP spec calls these commands
// and not requests.
@interface MNIMAPRequest : NSObject

@property(nonatomic, strong, readonly) MNIMAPRequestTicket *ticket;
@property(nonatomic, copy, readonly) id completionBlock;
@property(nonatomic, assign, readonly) MNIMAPRequestStatus status;
@property(nonatomic, copy, readonly) NSString *IMAPTag;
@property(nonatomic, copy, readonly) NSString *IMAPCommand;


+ (id)requestWithIMAPTag:(NSString *)IMAPTag andCompletionBlock:(id)completionBlock;
- (id)initWithIMAPTag:(NSString *)IMAPTag andCompletionBlock:(id)completionBlock;

// TODO: Consider placing state machine protection, i.e. can't go from In Progress
// back to Pending.
- (void)requestPending;
- (void)requestInProgress;
- (void)requestCompleteWithDataLines:(NSArray *)dataLines
                       andResultLine:(MNIMAPResponseLine *)resultLine;

@end
