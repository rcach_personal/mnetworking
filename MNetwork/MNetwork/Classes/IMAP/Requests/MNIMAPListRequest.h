#import "MNIMAPRequest.h"

@interface MNIMAPListRequest : MNIMAPRequest

@property(nonatomic, copy) NSString *referenceName;
// Mailbox name may contain wildcard characters.
@property(nonatomic, copy) NSString *mailboxName;

@end
