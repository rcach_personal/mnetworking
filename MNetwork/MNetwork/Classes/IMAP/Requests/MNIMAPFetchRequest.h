#import "MNIMAPRequest.h"

@interface MNIMAPFetchRequest : MNIMAPRequest

@property(nonatomic, copy) NSString *sequenceSet;
@property(nonatomic, copy) NSString *messageDataItemNamesOrMacro;

@end
