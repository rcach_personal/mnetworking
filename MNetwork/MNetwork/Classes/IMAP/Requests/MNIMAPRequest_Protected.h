#import "MNIMAPRequest.h"

#import "MNIMAPRequestTicket.h"

@interface MNIMAPRequest ()

// TODO: Might not need command string, it's not used in the base class.
+ (NSString *)commandString;
+ (MNIMAPRequestTicketType)ticketType;

- (NSString *)commandLine;
- (NSString *)endOfCommand;

@end
