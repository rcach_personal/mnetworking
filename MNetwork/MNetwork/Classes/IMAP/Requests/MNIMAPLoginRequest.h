#import "MNIMAPRequest.h"

@interface MNIMAPLoginRequest : MNIMAPRequest

// TODO: Rethink how command parameters are passed into the request objects.
@property(nonatomic, copy) NSString *username;
@property(nonatomic, copy) NSString *password;

@end
