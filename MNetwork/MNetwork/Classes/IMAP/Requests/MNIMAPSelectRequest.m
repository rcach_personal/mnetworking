#import "MNIMAPSelectRequest.h"

#import "MNIMAPClient.h"
#import "MNIMAPRequest_Protected.h"
#import "MNIMAPRequestTicket.h"

@implementation MNIMAPSelectRequest

+ (NSString *)commandString {
  return @"SELECT";
}

+ (MNIMAPRequestTicketType)ticketType {
  return MNIMAPRequestTicketTypeSelect;
}

- (NSString *)commandLine {
  NSArray *commandComponents = @[self.IMAPTag,
                                 [[self class] commandString],
                                 self.mailboxName];
  NSString *commandLine = [commandComponents componentsJoinedByString:@" "];
  return [commandLine stringByAppendingString:[self endOfCommand]];
}

- (void)requestCompleteWithDataLines:(NSArray *)dataLines
                       andResultLine:(MNIMAPResponseLine *)resultLine {
  [super requestCompleteWithDataLines:dataLines andResultLine:resultLine];
  if (self.completionBlock) {
    ((MNIMAPSelectResponse)self.completionBlock)();
  }
}

@end
