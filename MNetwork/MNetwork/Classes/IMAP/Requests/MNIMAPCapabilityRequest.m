#import "MNIMAPCapabilityRequest.h"

#import "MNIMAPClient.h"
#import "MNIMAPResponseLine.h"
#import "MNIMAPRequest_Protected.h"
#import "MNIMAPRequestTicket.h"

@implementation MNIMAPCapabilityRequest

+ (NSString *)commandString {
  return @"CAPABILITY";
}

+ (MNIMAPRequestTicketType)ticketType {
  return MNIMAPRequestTicketTypeCapability;
}

- (NSString *)commandLine {
  NSArray *commandComponents = @[self.IMAPTag,
                                 [[self class] commandString],
                                 [self endOfCommand]];
  return [commandComponents componentsJoinedByString:@" "];
}

- (void)requestCompleteWithDataLines:(NSArray *)dataLines
                       andResultLine:(MNIMAPResponseLine *)resultLine {
  [super requestCompleteWithDataLines:dataLines andResultLine:resultLine];
  if (self.completionBlock) {
    NSMutableArray *capabilities = [NSMutableArray array];
    if ([dataLines count] > 0) {
      MNIMAPResponseLine *capabilitiesResponseLine = dataLines[0];
      
      NSArray *responseComponents = capabilitiesResponseLine.components;
      [capabilities addObjectsFromArray:responseComponents];
      [capabilities removeObjectAtIndex:0];
      [capabilities removeObjectAtIndex:0];
    } else {
      // TODO: Handle no capabilities error.
    }
    ((MNIMAPCapabilityResponse)self.completionBlock)(capabilities, nil);
  }
}

@end
