#import "MNIMAPListRequest.h"

#import "MNIMAPClient.h"
#import "MNIMAPRequest_Protected.h"
#import "MNIMAPRequestTicket.h"

@implementation MNIMAPListRequest

+ (NSString *)commandString {
  return @"LIST";
}

+ (MNIMAPRequestTicketType)ticketType {
  return MNIMAPRequestTicketTypeList;
}

- (NSString *)commandLine {
  // TODO(rcacheaux): Implement empty space double quote injection.
  NSArray *commandComponents = @[self.IMAPTag,
                                 [[self class] commandString],
                                 self.referenceName,
                                 self.mailboxName];
  NSString *commandLine = [commandComponents componentsJoinedByString:@" "];
  return [commandLine stringByAppendingString:[self endOfCommand]];
}

- (void)requestCompleteWithDataLines:(NSArray *)dataLines
                       andResultLine:(MNIMAPResponseLine *)resultLine {
  [super requestCompleteWithDataLines:dataLines andResultLine:resultLine];
  if (self.completionBlock) {
    ((MNIMAPListResponse)self.completionBlock)();
  }
}

@end
