#import "MNIMAPFetchRequest.h"

#import "MNIMAPClient.h"
#import "MNIMAPRequest_Protected.h"
#import "MNIMAPRequestTicket.h"
#import "MNIMAPResponseLine.h"

@implementation MNIMAPFetchRequest

+ (NSString *)commandString {
  return @"FETCH";
}

+ (MNIMAPRequestTicketType)ticketType {
  return MNIMAPRequestTicketTypeFetch;
}

- (NSString *)commandLine {
  // TODO(rcacheaux): Move Tag and Command string prepend up into super.
  NSArray *commandComponents = @[self.IMAPTag,
                                 [[self class] commandString],
                                 self.sequenceSet,
                                 self.messageDataItemNamesOrMacro];
  // TODO(rcacheaux): Move this append up into super.
  NSString *commandLine = [commandComponents componentsJoinedByString:@" "];
  return [commandLine stringByAppendingString:[self endOfCommand]];
}

- (void)requestCompleteWithDataLines:(NSArray *)dataLines
                       andResultLine:(MNIMAPResponseLine *)resultLine {
  [super requestCompleteWithDataLines:dataLines andResultLine:resultLine];
  
  NSMutableString *subjectLine = [[NSMutableString alloc] init];
  subjectLine = [@"" mutableCopy];
  // TODO(rcacheaux): Make this more efficient.
  for (int i = 1; i < [dataLines count] - 1; i++) {
    MNIMAPResponseLine *line = dataLines[i];
    NSString *firstComponent = line.components[0];
    BOOL skipFirst = NO;
    if ([firstComponent isEqualToString:@"Subject:"]) {
      skipFirst = YES;
    }
    
    NSUInteger startingIndex = skipFirst ? 1 : 0;
    for (int i = startingIndex; i < [line.components count]; i++) {
      // TODO(rcacheaux): Total HACK. Implement support for data lines that are not to be
      // tokenized by space char.
      NSString *component = line.components[i];
      [subjectLine appendString:[component stringByAppendingString:@" "]];
    }
  }
  
  if (self.completionBlock) {
    ((MNIMAPFetchResponse)self.completionBlock)([subjectLine copy]);
  }
}

@end
