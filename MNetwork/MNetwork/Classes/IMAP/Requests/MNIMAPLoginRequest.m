#import "MNIMAPLoginRequest.h"

#import "MNIMAPClient.h"
#import "MNIMAPRequest_Protected.h"
#import "MNIMAPRequestTicket.h"

@implementation MNIMAPLoginRequest

+ (NSString *)commandString {
  return @"LOGIN";
}

+ (MNIMAPRequestTicketType)ticketType {
  return MNIMAPRequestTicketTypeLogin;
}

- (NSString *)commandLine {
  NSArray *commandComponents = @[self.IMAPTag,
                                 [[self class] commandString],
                                 self.username,
                                 [NSString stringWithFormat:@"\"%@\"",self.password]];
  NSString *commandString = [commandComponents componentsJoinedByString:@" "];
  return [commandString stringByAppendingString:[self endOfCommand]];
}

- (void)requestCompleteWithDataLines:(NSArray *)dataLines
                       andResultLine:(MNIMAPResponseLine *)resultLine {
  [super requestCompleteWithDataLines:dataLines andResultLine:resultLine];
  if (self.completionBlock) {
    ((MNIMAPLoginResponse)self.completionBlock)();
  }
}

@end
