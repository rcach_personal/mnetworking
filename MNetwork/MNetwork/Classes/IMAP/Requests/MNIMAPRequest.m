#import "MNIMAPRequest.h"

#import "MNIMAPRequest_Protected.h"
#import "MNIMAPRequestTicket.h"

@interface MNIMAPRequest ()
@property(nonatomic, strong, readwrite) MNIMAPRequestTicket *ticket;
@property(nonatomic, copy, readwrite) id completionBlock;
@property(nonatomic, assign, readwrite) MNIMAPRequestStatus status;
@property(nonatomic, copy, readwrite) NSString *IMAPTag;
@property(nonatomic, copy, readwrite) NSString *IMAPCommand;

@property(nonatomic, strong) NSMutableArray *IMAPResponses;
@property(nonatomic, copy) NSString *IMAPStatusResponse;
@end

@implementation MNIMAPRequest

+ (NSString *)commandString {
//  MNAbstractMethod;
  return nil;
}

+ (MNIMAPRequestTicketType)ticketType {
//  MNAbstractMethod;
  return MNIMAPRequestTicketTypeUknown;
}

+ (id)requestWithIMAPTag:(NSString *)IMAPTag andCompletionBlock:(id)completionBlock {
  return [[self alloc] initWithIMAPTag:IMAPTag andCompletionBlock:completionBlock];
}

- (id)initWithIMAPTag:(NSString *)IMAPTag andCompletionBlock:(id)completionBlock {
  self = [super init];
//  MNAbstractClass(MNIMAPRequest);
  if (self) {
    self.IMAPTag = IMAPTag;
    self.completionBlock = completionBlock;
    self.status = MNIMAPRequestStatusUnknown;
    self.IMAPResponses = [NSMutableArray array];
    self.ticket =[MNIMAPRequestTicket ticketWithID:self.IMAPTag
                                           andType:[[self class] ticketType]];
  }
  return self;
}

- (id)init {
  self = [super init];
  if (self) {
    MNInvalidateInitializer();
  }
  return self;
}

- (void)requestPending {
  self.status = MNIMAPRequestStatusPending;
}

- (void)requestInProgress {
  self.status = MNIMAPRequestStatusInProgress;
}

- (void)requestCompleteWithDataLines:(NSArray *)dataLines
                       andResultLine:(MNIMAPResponseLine *)resultLine {
  self.status = MNIMAPRequestStatusComplete;
}

// TODO: Figure out a better way to do this, defered b.c. some request sublclasses
// have arguments and they need to be set before constructing the command line.
- (NSString *)IMAPCommand {
  if (!_IMAPCommand) {
    _IMAPCommand = [self commandLine];
  }
  // TODO: Use command OR request, don't mix vocab.
  return _IMAPCommand;
}

- (NSString *)commandLine {
//  MNAbstractMethod;
  return nil;
}

// TODO: This needs to be re-used elsewhere, place somewhere more accessible.
- (NSString *)endOfCommand {
  return @"\r\n";
}

@end
