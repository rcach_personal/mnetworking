#import "MNIMAPSearchRequest.h"

#import "MNIMAPClient.h"
#import "MNIMAPRequest_Protected.h"
#import "MNIMAPRequestTicket.h"
#import "MNIMAPResponseLine.h"

@implementation MNIMAPSearchRequest

+ (NSString *)commandString {
  return @"SEARCH";
}

+ (MNIMAPRequestTicketType)ticketType {
  return MNIMAPRequestTicketTypeSearch;
}

- (NSString *)commandLine {
  // TODO(rcacheaux): Implement empty space double quote injection.
  NSArray *commandComponents = @[self.IMAPTag,
                                 [[self class] commandString],
                                 self.searchingCriteria];
  NSString *commandLine = [commandComponents componentsJoinedByString:@" "];
  return [commandLine stringByAppendingString:[self endOfCommand]];
}

- (void)requestCompleteWithDataLines:(NSArray *)dataLines
                       andResultLine:(MNIMAPResponseLine *)resultLine {
  [super requestCompleteWithDataLines:dataLines andResultLine:resultLine];
  
  NSAssert([dataLines count] > 0, @"Search response should include one data line.");
  MNIMAPResponseLine *searchResultResponseLine = dataLines[0];
  NSUInteger searchStringIndex =
      [searchResultResponseLine.components indexOfObject:[[self class] commandString]];
  NSUInteger numberOfComponents = [searchResultResponseLine.components count];
  NSMutableArray *messageSequenceNumbers = [NSMutableArray array];
  for (int i = searchStringIndex + 1; i < numberOfComponents; ++i) {
    [messageSequenceNumbers addObject:searchResultResponseLine.components[i]];
  }
  if (self.completionBlock) {
    ((MNIMAPListResponse)self.completionBlock)(messageSequenceNumbers);
  }
}

@end
