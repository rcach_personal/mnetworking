#import "MNIMAPRequest.h"

@interface MNIMAPSelectRequest : MNIMAPRequest

@property(nonatomic, copy) NSString *mailboxName;

@end
