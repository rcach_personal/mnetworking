#import "MNIMAPRequest.h"

@interface MNIMAPSearchRequest : MNIMAPRequest

@property(nonatomic, copy) NSString *searchingCriteria;

@end
