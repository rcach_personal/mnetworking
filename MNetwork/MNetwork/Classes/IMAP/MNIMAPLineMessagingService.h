// TODO: Not being used yet, evaluate whether it is a good idea.
#import "MNNetworkConnection.h"

typedef void (^MNIMAPLineReceivedHandler)(NSString *line);

@interface MNIMAPLineMessagingService : NSObject<MNNetworkConnectionEventHandler>

+ (id)lineMessagingServiceWithConnection:(id<MNNetworkConnection>)connection
                  andLineReceivedHandler:(MNIMAPLineReceivedHandler)lineReceived;

- (id)initWithConnection:(id<MNNetworkConnection>)connection
  andLineReceivedHandler:(MNIMAPLineReceivedHandler)lineReceivedHandler;

- (void)sendLine:(NSString *)line;

@end
