#import "MNIMAPLineMessagingService.h"

#import "MNNetworkConnection.h"

@interface MNIMAPLineMessagingService ()
@property(nonatomic, strong) id<MNNetworkConnection> connection;
@property(nonatomic, copy) MNIMAPLineReceivedHandler lineReceivedHandler;
@end

@implementation MNIMAPLineMessagingService

- (id)initWithConnection:(id<MNNetworkConnection>)connection
  andLineReceivedHandler:(MNIMAPLineReceivedHandler)lineReceivedHandler {
  
  self = [super init];
  if (self) {
    self.connection = connection;
    self.connection.eventHandler = self;
    self.lineReceivedHandler = lineReceivedHandler;
  }
  return self;
}

- (id)init {
  self = [super init];
  if (self) {
    MNInvalidateInitializer();
  }
  return self;
}




@end
