#import "MNIMAPClient.h"

#import <CocoaLumberjack/DDLog.h>

#import "MNNetworkConnection.h"
#import "MNIMAPRequestTicket.h"
#import "MNIMAPRequest.h"

#import "MNIMAPResponseLine.h"
// TODO(rcacheaux): Create single header for all request types.
#import "MNIMAPCapabilityRequest.h"
#import "MNIMAPLoginRequest.h"
#import "MNIMAPSelectRequest.h"
#import "MNIMAPListRequest.h"
#import "MNIMAPSearchRequest.h"
#import "MNIMAPFetchRequest.h"

#import "NSString+MNUUID.h"

static const int ddLogLevel = LOG_LEVEL_INFO;

@interface MNIMAPClient ()<MNNetworkConnectionEventHandler>
@property(nonatomic, strong) id<MNNetworkConnection> networkConnection;
@property(nonatomic, strong) NSMutableArray *requestQueue;
@property(nonatomic, strong) MNIMAPRequest *currentRequest;
@property(nonatomic, assign) BOOL receivedServerGreeting;
@property(nonatomic, assign) NSUInteger tagIncrementor;

@property(nonatomic, strong) NSMutableArray *currentResponseLines;

@property(nonatomic, assign) BOOL connected;
@end

// Vocab:
// Server and Client send lines.
//
// TODO(rcacheaux): Build in state of connection Auth,Selected, etc.
// TODO(rcacheaux): Build in selected mailbox state.
@implementation MNIMAPClient

+ (id)clientWithNetworkConnectionFactory:(MNNetworkConnectionFactory)connectionFactory {
  return [[self alloc] initWithNetworkConnectionFactory:connectionFactory];
}

- (id)initWithNetworkConnectionFactory:(MNNetworkConnectionFactory)connectionFactory {
  self = [super init];
  if (self) {
    MNAssert(connectionFactory != nil,
             @"Network connection factory for IMAP client must not be nil.");
    self.connected = NO;
    self.receivedServerGreeting = NO;
    self.tagIncrementor = 0;
    self.currentResponseLines = [NSMutableArray array];
    self.requestQueue = [NSMutableArray array];
    self.networkConnection = connectionFactory != nil ? connectionFactory() : nil;
    self.networkConnection.eventHandler = self;
    [self.networkConnection connect];
  }
  return self;
}

- (id)init {
  self = [super init];
  if (self) {
    MNInvalidateInitializer();
  }
  return self;
}

#pragma mark - Request Queue

// TODO: This suggests queue is its own class.
- (void)addRequestToQueue:(MNIMAPRequest *)request {
  DDLogVerbose(@"IMAP Client: Adding IMAP request, %@, to queue.", request);
  [self.requestQueue addObject:request];
  [request requestPending];
  if (!self.currentRequest && self.receivedServerGreeting) {
    [self handleNextRequest];
  }
}

- (void)handleNextRequest {
  DDLogVerbose(@"IMAP Client: Handle next request.");
  self.currentRequest = [self nextRequestInQueue];
  if (!self.currentRequest) {
    DDLogVerbose(@"IMAP Client: No request pending to handle.");
    return;
  }
  NSData *requestData =
      [self.currentRequest.IMAPCommand dataUsingEncoding:NSASCIIStringEncoding];
  DDLogVerbose(@"IMAP Client: Sending Command: %@", self.currentRequest.IMAPCommand);
  [self.networkConnection sendData:requestData];
  [self.currentRequest requestInProgress];
}

- (MNIMAPRequest *)nextRequestInQueue {
  for (MNIMAPRequest *request in self.requestQueue) {
    if (request.status == MNIMAPRequestStatusPending) {
      return request;
    }
  }
  return nil;
}

- (MNIMAPRequest *)requestWithTag:(NSString *)tag {
  for (MNIMAPRequest *request in self.requestQueue) {
    if ([request.IMAPTag isEqualToString:tag]) {
      return request;
    }
  }
  return nil;
}

#pragma mark - IMAP Protocol Utilities

- (NSString *)nextTag {
  self.tagIncrementor++;
  // TODO: Do NOT use this implementation.
  return [[NSString mn_newUUID]
          stringByReplacingCharactersInRange:NSMakeRange(4, 32) withString:@""];;
}

- (NSString *)endOfCommand {
  return @"\r\n";
}

#pragma mark - IMAP Protocol

- (MNIMAPRequestTicket *)capability:(MNIMAPCapabilityResponse)capabilityResonse {
  MNIMAPCapabilityRequest *request =
      [MNIMAPCapabilityRequest requestWithIMAPTag:[self nextTag]
                               andCompletionBlock:capabilityResonse];
  [self addRequestToQueue:request];
  return request.ticket;
}

- (MNIMAPRequestTicket *)login:(MNIMAPLoginResponse)loginResponse
                  withUsername:(NSString *)username
                   andPassword:(NSString *)password {  
  // Create request.
  MNIMAPLoginRequest *request = [MNIMAPLoginRequest requestWithIMAPTag:[self nextTag]
                                                    andCompletionBlock:loginResponse];
  request.username = username;
  request.password = password;
  [self addRequestToQueue:request];
  return request.ticket;
}

- (MNIMAPRequestTicket *)list:(MNIMAPListResponse)listResponse
                referenceName:(NSString *)referenceName
                  mailboxName:(NSString *)mailboxName {
  MNIMAPListRequest *request = [MNIMAPListRequest requestWithIMAPTag:[self nextTag]
                                                  andCompletionBlock:listResponse];
  request.referenceName = referenceName;
  request.mailboxName = mailboxName;
  [self addRequestToQueue:request];
  return request.ticket;
}

- (MNIMAPRequestTicket *)select:(MNIMAPSelectResponse)selectResponse
                        mailbox:(NSString *)mailbox {
  MNIMAPSelectRequest *request = [MNIMAPSelectRequest requestWithIMAPTag:[self nextTag]
                                                      andCompletionBlock:selectResponse];
  request.mailboxName = mailbox;
  [self addRequestToQueue:request];
  return request.ticket;
}

- (MNIMAPRequestTicket *)search:(MNIMAPSearchResponse)searchResponse
                 searchCriteria:(NSString *)searchingCriteria {
  MNIMAPSearchRequest *request = [MNIMAPSearchRequest requestWithIMAPTag:[self nextTag]
                                                      andCompletionBlock:searchResponse];
  request.searchingCriteria = searchingCriteria;
  [self addRequestToQueue:request];
  return request.ticket;
}

- (MNIMAPRequestTicket *)fetch:(MNIMAPFetchResponse)fetchResponse
                   sequenceSet:(NSString *)sequenceSet
   messageDataItemNamesOrMacro:(NSString *)messageDataItemNamesOrMacro {
  MNIMAPFetchRequest *request = [MNIMAPFetchRequest requestWithIMAPTag:[self nextTag]
                                                    andCompletionBlock:fetchResponse];
  request.sequenceSet = sequenceSet;
  request.messageDataItemNamesOrMacro = messageDataItemNamesOrMacro;
  [self addRequestToQueue:request];
  return request.ticket;
}

#pragma mark - MNNetworkConnection Event Handler

- (void)connectionDidConnect:(id<MNNetworkConnection>)connection {
  self.connected = YES;
  if (self.receivedServerGreeting) {
    [self handleNextRequest];
  }
}

// TODO: Add awareness of IMAP connection state:
// - Not Authenticated State
// - Authenticated State
// - Selected State
// - Logout State

// @"OK", @"NO", @"BAD" - IMAP Response Statuses

- (void)connectionDidDisconnect:(id<MNNetworkConnection>)connection
                          error:(NSError *)error {
  
}

- (void)connectionDidNotConnect:(id<MNNetworkConnection>)connection
                          error:(NSError *)error {
  
}

- (void)connection:(id<MNNetworkConnection>)connection didReceiveData:(NSData *)response {
  // TODO: Is it OK to assume UTF8 encoded payload?
  // TODO: Handle situation where a single response results in multiple call backs.
  NSString *responseString = [[NSString alloc] initWithData:response
                                                   encoding:NSUTF8StringEncoding];
  NSArray *responseLines =
      [responseString componentsSeparatedByString:[self endOfCommand]];
  NSUInteger numberOfResponseLines = [responseLines count];
  if (numberOfResponseLines == 1) {
    DDLogVerbose(@"IMAP Client: One complete response line");
    DDLogVerbose(@"IMAP Client: Connection Response: %@", responseString);
  } else if (numberOfResponseLines > 1) {
    DDLogVerbose(@"IMAP Client: More than one response line");
    // Last component will always be empty if last character is CRLF.
    for (int i = 0; i < numberOfResponseLines; i++) {
      if (![responseLines[i] isEqualToString:@""]) {
        DDLogVerbose(@"IMAP Client: Response %d: %@", i + 1, responseLines[i]);
        MNIMAPResponseLine *responseLine = [MNIMAPResponseLine
                                            responseLineWithString:responseLines[i]];
        [self handleResponseLine:responseLine];
      }
    }
  } else {
    DDLogVerbose(@"IMAP Client: No or error in response");
  }
}

- (void)handleResponseLine:(MNIMAPResponseLine *)responseLine {
  // Must go until receiev a result ending in a CRLF.
  // You know you're done when you read a result block and the last entry in responses
  // is an empty string, otherwise wait for the end.
  
  // status response
  DDLogVerbose(@"IMAP Client: Handle Response Line: %@", responseLine);
  if (!self.receivedServerGreeting) {
    if ([responseLine isServerGreeting]) {
      DDLogVerbose(@"IMAP Client: Received Server Greeting Response Line: %@",
                   responseLine);
      self.receivedServerGreeting = YES;
      if (self.connected) {
        [self handleNextRequest];
      }
      return;
    }
  }
  
  // TODO: This doesn't check the status response's IMAP tag.
  if ([responseLine isResultLine]) {
    // Identify which request this belongs to.
    DDLogVerbose(@"IMAP Client: Received result line, %@, with tag, %@",
                 responseLine,
                 responseLine.tag);

    MNIMAPRequest *requestForResultLine = [self requestWithTag:responseLine.tag];

    
    if (!requestForResultLine) {
      // TODO: Should this clear out self.currentResponseLines?
      MNFail(@"Result, %@, should match a known request.", responseLine);
      return;
    }
    if (requestForResultLine != self.currentRequest) {
      // TODO: Should this clear out self.currentResponseLines?
      DDLogError(@"IMAP Client: FATAL Request queue might get stalled indefinitely.");
      MNFail(@"Requests should be executed serially. "
             @"Response received, %@, does not match current request, %@.",
             responseLine,
             self.currentRequest);
      return;
    }
    
    [self.currentRequest requestCompleteWithDataLines:self.currentResponseLines
                                        andResultLine:responseLine];
    self.currentRequest = nil;
    [self.currentResponseLines removeAllObjects];
    DDLogVerbose(@"IMAP Client: Command DONE, Go to Next!");
    [self handleNextRequest];
  } else {
    DDLogVerbose(@"IMAP Client: Received Response Data Line: %@",
                 responseLine);
    DDLogVerbose(@"IMAP Client: Adding response line, %@, to current response lines.",
                 responseLine);
    [self.currentResponseLines addObject:responseLine];
  }
}

@end
