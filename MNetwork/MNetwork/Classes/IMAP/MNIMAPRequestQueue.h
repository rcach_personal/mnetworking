@class MNIMAPRequest;

typedef void (^MNSendRequest)(MNIMAPRequest *request);

@interface MNIMAPRequestQueue : NSObject

- (void)addRequestToQueue:(MNIMAPRequest *)request;
- (MNIMAPRequest *)nextRequestInQueue;
- (MNIMAPRequest *)requestWithTag:(NSString *)tag;
- (void)handleNextRequest;

@end
