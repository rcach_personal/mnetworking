#import "MNNetworkConnection.h"

@class MNTCPConnection;
@class MNIMAPRequestTicket;

// TODO: Should these declarations go elsewhere?
typedef void (^MNIMAPCapabilityResponse)(NSArray *capabilities, NSError *error);
typedef void (^MNIMAPLoginResponse)();
typedef void (^MNIMAPAuthenticateResponse)();
typedef void (^MNIMAPListResponse)();
typedef void (^MNIMAPCreateResponse)();
typedef void (^MNIMAPDeleteResponse)();
typedef void (^MNIMAPRenameResponse)();
typedef void (^MNIMAPStatusResponse)();
typedef void (^MNIMAPSelectResponse)();
typedef void (^MNIMAPSearchResponse)(NSArray *messageSequenceNumbers);
typedef void (^MNIMAPFetchResponse)(NSString *messageSubject);
typedef void (^MNIMAPCloseResponse)();
typedef void (^MNIMAPLogoutResponse)();

@interface MNIMAPClient : NSObject

+ (id)clientWithNetworkConnectionFactory:(MNNetworkConnectionFactory)connectionFactory;

- (id)initWithNetworkConnectionFactory:(MNNetworkConnectionFactory)connectionFactory;

- (MNIMAPRequestTicket *)capability:(MNIMAPCapabilityResponse)capabilityResonse;

- (MNIMAPRequestTicket *)login:(MNIMAPLoginResponse)loginResponse
                  withUsername:(NSString *)username
                   andPassword:(NSString *)password;

- (MNIMAPRequestTicket *)authenticate:(MNIMAPAuthenticateResponse)authenticateResponse
                        withMechanism:(NSString *)mechanism
                   andInitialResponse:(NSString *)initialResponse;

- (MNIMAPRequestTicket *)list:(MNIMAPListResponse)listResponse
                referenceName:(NSString *)referenceName
                  mailboxName:(NSString *)mailboxName;

- (MNIMAPRequestTicket *)create:(MNIMAPCreateResponse)createResponse
                        mailbox:(NSString *)mailbox;

- (MNIMAPRequestTicket *)delete:(MNIMAPDeleteResponse)deleteResponse
                        mailbox:(NSString *)mailbox;

- (MNIMAPRequestTicket *)rename:(MNIMAPRenameResponse)renameResponse
                    mailboxFrom:(NSString *)fromName
                             to:(NSString *)toName;

- (MNIMAPRequestTicket *)status:(MNIMAPStatusResponse)statusResponse
                     forMailbox:(NSString *)mailbox;

- (MNIMAPRequestTicket *)select:(MNIMAPSelectResponse)selectResponse
                        mailbox:(NSString *)mailbox;

- (MNIMAPRequestTicket *)search:(MNIMAPSearchResponse)searchResponse
                 searchCriteria:(NSString *)searchingCriteria;

- (MNIMAPRequestTicket *)fetch:(MNIMAPFetchResponse)fetchResponse
                   sequenceSet:(NSString *)sequenceSet
   messageDataItemNamesOrMacro:(NSString *)messageDataItemNamesOrMacro;

- (MNIMAPRequestTicket *)close:(MNIMAPCloseResponse)closeResponse;

- (MNIMAPRequestTicket *)logout:(MNIMAPLogoutResponse)logoutResponse;

@end
