#import "MNIMAPResponseLine.h"

@interface MNIMAPResponseLine ()
@property(nonatomic, assign, readwrite) NSUInteger numberOfComponents;
@property(nonatomic, copy, readwrite) NSArray *components;
@property(nonatomic, copy, readwrite) NSString *tag;

@property(nonatomic, copy) NSString *lineString;
@end

static NSString * const kIMAPResponseDataInidcator = @"*";

@implementation MNIMAPResponseLine

+ (id)responseLineWithString:(NSString *)string {
  return [[self alloc] initWithString:string];
}

- (id)initWithString:(NSString *)string {
  self = [super init];
  if (self) {
    self.lineString = string;
    self.components = [self lineComponentsFromString:string];
    self.tag = [self tagFromLineComponents:self.components];
  }
  return self;
}

- (id)init {
  self = [super init];
  if (self) {
    MNInvalidateInitializer();
  }
  return self;
}

- (NSArray *)lineComponentsFromString:(NSString *)string {
  return [string componentsSeparatedByString:@" "];
}

- (NSString *)tagFromLineComponents:(NSArray *)lineComponents {
  if ([lineComponents count] > 0) {
    NSString *tagCandidate = lineComponents[0];
    if (![tagCandidate isEqualToString:kIMAPResponseDataInidcator]) {
      return tagCandidate;
    }
  }
  return @"";
}

- (BOOL)isServerGreeting {
  if (self.numberOfComponents >= 2) {
    NSString *firstComponent = self.components[0];
    NSString *status = self.components[1];
    if ([firstComponent isEqualToString:kIMAPResponseDataInidcator] &&
        [status isEqualToString:@"OK"]) {
      return YES;
    }
  }
  return NO;
}

// TODO(rcacheaux): Cleanup.
- (BOOL)isResultLine {
  if (self.numberOfComponents >= 2) {
    NSString *firstComponent = self.components[0];
    if ([firstComponent isEqualToString:kIMAPResponseDataInidcator]) {
      return NO;
    } else if ([self componentIsStatus:self.components[1]]) {
      // TODO: Check for @"OK", @"NO", @"BAD" - IMAP Response Statuses.
      return YES;
    } else {
      return NO;
    }
  }
  // TODO: ?.
  return NO;
}

// TODO(rcacheaux): Cleanup.
- (BOOL)componentIsStatus:(NSString *)component {
  if ([component isEqualToString:@"OK"]) {
    return YES;
  } else if ([component isEqualToString:@"NO"]) {
    return YES;
  } else if ([component isEqualToString:@"BAD"]) {
    return YES;
  } else {
    return NO;
  }
}

- (NSUInteger)numberOfComponents {
  return [self.components count];
}

@end
