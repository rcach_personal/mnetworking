
// TODO: Consider creating a base line class and have subclasses such as requestLine,
// resultLine, responseLine, etc. Use a line factory to create the right one.
@interface MNIMAPResponseLine : NSObject

@property(nonatomic, assign, readonly) NSUInteger numberOfComponents;
@property(nonatomic, copy, readonly) NSArray *components;
@property(nonatomic, copy, readonly) NSString *tag;

+ (id)responseLineWithString:(NSString *)string;
- (id)initWithString:(NSString *)string;

- (BOOL)isServerGreeting;
- (BOOL)isResultLine;

@end
