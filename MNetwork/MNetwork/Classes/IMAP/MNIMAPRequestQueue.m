#import "MNIMAPRequestQueue.h"

#import <CocoaLumberjack/DDLog.h>

#import "MNIMAPRequest.h"

static const int ddLogLevel = LOG_LEVEL_VERBOSE;

@interface MNIMAPRequestQueue ()
@property(nonatomic, strong) NSMutableArray *requestQueue;
@property(nonatomic, copy) MNSendRequest sendRequest;
@property(nonatomic, strong) MNIMAPRequest *currentRequest;
@end

@implementation MNIMAPRequestQueue

- (id)init {
  self = [super init];
  if (self) {
    self.requestQueue = [NSMutableArray array];
  }
  return self;
}

- (void)addRequestToQueue:(MNIMAPRequest *)request {
  DDLogVerbose(@"IMAP Request Queue: Adding IMAP request, %@, to queue.", request);
  [self.requestQueue addObject:request];
  [request requestPending];
  if (!self.currentRequest) {// && self.receivedServerGreeting) {
    [self handleNextRequest];
  }
}

- (MNIMAPRequest *)nextRequestInQueue {
  for (MNIMAPRequest *request in self.requestQueue) {
    if (request.status == MNIMAPRequestStatusPending) {
      return request;
    }
  }
  return nil;
}

- (MNIMAPRequest *)requestWithTag:(NSString *)tag {
  
}

- (void)handleNextRequest {
  DDLogVerbose(@"IMAP Request Queue: Handle next request.");
  self.currentRequest = [self nextRequestInQueue];
  if (!self.currentRequest) {
    DDLogVerbose(@"IMAP Request Queue: No request pending to handle.");
    return;
  }
  
  if (self.sendRequest) {
    self.sendRequest(self.currentRequest);
  } else {
    MNFail(@"IMAP Request Queue: Must have a send request block");
  }
  //  NSData *requestData =
  //  [self.currentRequest.IMAPCommand dataUsingEncoding:NSUTF8StringEncoding];
  //  DDLogVerbose(@"IMAP Request Queue: Sending Command: %@",
  //               self.currentRequest.IMAPCommand);
  //  [self.networkConnection sendData:requestData];
  [self.currentRequest requestInProgress];
}

@end
