typedef NS_ENUM(NSUInteger, MNIMAPRequestTicketType) {
  MNIMAPRequestTicketTypeUknown,
  MNIMAPRequestTicketTypeCapability,
  MNIMAPRequestTicketTypeLogin,
  MNIMAPRequestTicketTypeAuthenticate,
  MNIMAPRequestTicketTypeSelect,
  MNIMAPRequestTicketTypeList,
  MNIMAPRequestTicketTypeSearch,
  MNIMAPRequestTicketTypeFetch
};

@interface MNIMAPRequestTicket : NSObject

@property(nonatomic, copy, readonly) NSString *ID;
@property(nonatomic, assign, readonly) MNIMAPRequestTicketType type;

+ (id)ticketWithID:(NSString *)ID andType:(MNIMAPRequestTicketType)type;
- (id)initWithID:(NSString *)ID andType:(MNIMAPRequestTicketType)type;

@end
