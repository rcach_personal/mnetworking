#import "MNIMAPRequestTicket.h"

@interface MNIMAPRequestTicket ()
@property(nonatomic, copy, readwrite) NSString *ID;
@property(nonatomic, assign, readwrite) MNIMAPRequestTicketType type;
@end

@implementation MNIMAPRequestTicket

+ (id)ticketWithID:(NSString *)ID andType:(MNIMAPRequestTicketType)type {
  return [[self alloc] initWithID:ID andType:type];
}

- (id)init {
  self = [super init];
  if (self) {
    MNInvalidateInitializer();
  }
  return self;
}

- (id)initWithID:(NSString *)ID andType:(MNIMAPRequestTicketType)type {
  self = [super init];
  if (self) {
    self.ID = ID;
    self.type = type;
  }
  return self;
}

@end
