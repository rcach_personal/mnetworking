#import "MNMockingTestCase.h"

#import <OCMock/OCMock.h>

@interface MNMockingTestCase ()
@property(nonatomic, strong) NSMutableDictionary *mocks;
@end

@implementation MNMockingTestCase

- (void)invokeTest {
  self.mocks = [NSMutableDictionary dictionary];
  @try {
    [super invokeTest];
  }
  @catch (NSException *exception) {
    self.mocks = nil;
    @throw exception;
  }
  
  @try {
    [self verifyAndReset];
  }
  @catch (NSException *exception) {
    self.mocks = nil;
    @throw exception;
  }
  
  self.mocks = nil;
}

- (void)verifyAndReset {
  NSArray *allMocks = [self.mocks allValues];
  for (id mock in allMocks) {
    [mock verify];
    // |verify| asserts that the expectation queue is empty, which implies 'reset.'
  }
}

- (id)mockForProperty:(NSString *)propertyName withClass:(Class)clazz {
  id value = [self.mocks objectForKey:propertyName];
  if (value == nil) {
    value = [OCMockObject mockForClass:clazz];
    [self.mocks setValue:value forKey:propertyName];
  }
  return value;
}

- (id)mockForProperty:(NSString *)propertyName withProtocol:(Protocol *)protocol {
  id value = [self.mocks objectForKey:propertyName];
  if (value == nil) {
    value = [OCMockObject mockForProtocol:protocol];
    [self.mocks setValue:value forKey:propertyName];
  }
  return value;
}

- (id)niceMockForProperty:(NSString *)propertyName withClass:(Class)clazz {
  id value = [self.mocks objectForKey:propertyName];
  if (value == nil) {
    value = [OCMockObject niceMockForClass:clazz];
    [self.mocks setValue:value forKey:propertyName];
  }
  return value;
}

- (id)niceMockForProperty:(NSString *)propertyName withProtocol:(Protocol *)protocol {
  id value = [self.mocks objectForKey:propertyName];
  if (value == nil) {
    value = [OCMockObject niceMockForProtocol:protocol];
    [self.mocks setValue:value forKey:propertyName];
  }
  return value;
}

@end
