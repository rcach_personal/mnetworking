#import <SenTestingKit/SenTestingKit.h>

// MOCK() is used to indicate fields on a classs that should be replaced with mock objects
// through reflection.
#ifndef MOCK
#define DECLARE_MOCK(obj) @property(readonly, nonatomic) id obj;
#define MOCK_PROTOCOL(obj, protzz) \
  - (id)obj { \
    return [self mockForProperty:@#obj withProtocol:@protocol(protzz)]; \
  }
#define MOCK(obj, clazz) \
  - (id)obj { \
    return [self mockForProperty:@#obj withClass:[clazz class]]; \
  }
#define NICE_MOCK_PROTOCOL(obj, protzz) \
  - (id)obj { \
    return [self niceMockForProperty:@#obj withProtocol:@protocol(protzz)]; \
  }
#define NICE_MOCK(obj, clazz) \
  - (id)obj { \
    return [self niceMockForProperty:@#obj withClass:[clazz class]]; \
  }
#endif

// For blocks passed into [mock andDo:].
typedef void (^InvocationBlock)(NSInvocation *);


@interface MNMockingTestCase : SenTestCase

// Verify that all method calls expected by mocks known to this test or throw error.
// After being called, this method resets all the mocks into record mode and clears
// their expectations.
- (void)verifyAndReset;

// Returns a mock for the specified property. The mock will be created if it has not
// already been initialized.
- (id)mockForProperty:(NSString *)propertyName withClass:(Class)clazz;

// Returns a mock for the specified property. The mock will be created if it has not
// already been initialized.
- (id)mockForProperty:(NSString *)propertyName withProtocol:(Protocol *)protocol;

// Returns a nice mock for the specified property. The mock will be created if it has not
// already been initialized.
- (id)niceMockForProperty:(NSString *)propertyName withClass:(Class)clazz;

// Returns a nice mock for the specified property. The mock will be created if it has not
// already been initialized.
- (id)niceMockForProperty:(NSString *)propertyName withProtocol:(Protocol *)protocol;

@end
