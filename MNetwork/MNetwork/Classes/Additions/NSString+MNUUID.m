#import "NSString+MNUUID.h"

@implementation NSString (MNUUID)

+ (NSString *)mn_newUUID {
  CFUUIDRef uuidRef = CFUUIDCreate(NULL);
  NSString *uuid = (__bridge_transfer NSString *)CFUUIDCreateString(NULL, uuidRef);
  CFRelease(uuidRef);
  return uuid;
}

@end
