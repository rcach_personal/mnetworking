#import "MNGMail.h"

#import <CocoaLumberjack/DDLog.h>
#import <CocoaLumberjack/DDASLLogger.h>
#import <CocoaLumberjack/DDTTYLogger.h>

#import "MNIMAPClient.h"
#import "MNNetworkConnection.h"
#import "MNTCPConnection.h"
#import "MNTCPHost.h"

@implementation MNGMail

static const int ddLogLevel = LOG_LEVEL_INFO;

+ (void)loginAndFetchTodaysMessageSubjectLinesWithUsername:(NSString *)username
                                                  password:(NSString *)password
                                                  response:(MNGMailResponse)response {
  
  MNIMAPClient *imap = [MNIMAPClient clientWithNetworkConnectionFactory:^{
    MNTCPHost *gmailIMAPHost = [MNTCPHost hostWithName:@"imap.gmail.com"];
    NSUInteger TLSIMAPPortNumber = 993;
    return [MNTCPConnection connectionWithHost:gmailIMAPHost onPort:TLSIMAPPortNumber];
  }];
  
  [imap capability:^(NSArray *capabilities, NSError *error){
    for (NSString *capability in capabilities) {
      DDLogInfo(@"Capability: %@", capability);
    }
  }];
  
  MNIMAPLoginResponse loginResponse = ^{
    DDLogInfo(@"Logged In!");
    [imap select:^{
      DDLogInfo(@"Selected Inbox!");
      [imap search:^(NSArray *messageSequenceNumbers){
        DDLogVerbose(@"Today's Messages: %@", messageSequenceNumbers);
        
        NSMutableArray *subjectLines = [NSMutableArray array];
        for (NSString *messageSequenceNumber in messageSequenceNumbers) {
          [imap fetch:^(NSString *subject){
            DDLogVerbose(@"Subject:\n%@", subject);
            [subjectLines addObject:subject];
          }
                   sequenceSet:messageSequenceNumber
   messageDataItemNamesOrMacro:@"BODY.PEEK[HEADER.FIELDS (SUBJECT)]"];
          
          if ([messageSequenceNumber
               isEqualToString:[messageSequenceNumbers lastObject]]) {
            response ? response(subjectLines) : NULL;
          }
          
        }
      } searchCriteria:@"SENTON 13-Apr-2013"];
      
    } mailbox:@"INBOX"];
  };
  
  [imap login:loginResponse
      withUsername:username
       andPassword:password];
}

@end
