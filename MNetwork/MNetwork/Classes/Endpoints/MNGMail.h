#import <Foundation/Foundation.h>

typedef void (^MNGMailResponse)(NSArray *subjectLines);

@interface MNGMail : NSObject

+ (void)loginAndFetchTodaysMessageSubjectLinesWithUsername:(NSString *)username
                                                  password:(NSString *)password
                                                  response:(MNGMailResponse)response;

@end
