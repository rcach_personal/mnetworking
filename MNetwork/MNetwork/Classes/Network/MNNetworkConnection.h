@protocol MNNetworkConnection;
@protocol MNNetworkConnectionEventHandler;

typedef id<MNNetworkConnection> (^MNNetworkConnectionFactory)();

@protocol MNNetworkConnection <NSObject>

@property(nonatomic, assign) id<MNNetworkConnectionEventHandler> eventHandler;
@property(nonatomic, assign, readonly) BOOL connected;
@property(nonatomic, assign, readonly) BOOL errorConnecting;
@property(nonatomic, strong, readonly) NSError *connectionError;

- (void)connect;
- (BOOL)sendData:(NSData *)request;
- (void)disconnect;

@end

@protocol MNNetworkConnectionEventHandler <NSObject>

- (void)connectionDidConnect:(id<MNNetworkConnection>)connection;
- (void)connectionDidDisconnect:(id<MNNetworkConnection>)connection
                          error:(NSError *)error;
- (void)connectionDidNotConnect:(id<MNNetworkConnection>)connection
                          error:(NSError *)error;
- (void)connection:(id<MNNetworkConnection>)connection didReceiveData:(NSData *)data;

@end