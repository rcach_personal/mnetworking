#import <UIKit/UIKit.h>

@class MNViewController;

@interface MNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) MNViewController *viewController;

@end
